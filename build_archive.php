<?php
    chdir(__DIR__);	// cron等で動かすとき用にカレントディレクトリをこのファイルのディレクトリーに指定

    include('load_api.php');
    require('manage_archive.php');

    define('LEMON_BEADS', 1234240856);	// レモビさんのuserId
    define('ARCHIVE_FILE_NAME', 'lemon_beads.json');	// ログのファイル名


    $temp = $connection -> get('statuses/user_timeline', [ 'user_id' => LEMON_BEADS, 'count' => 1 ]);
    $beforeMax = $temp[0] -> id;
    $beforeMin = null;
    for ($i=0; (($result[$i] = $connection -> get('statuses/user_timeline', [ 'user_id' => LEMON_BEADS, 'count' => 200, 'max_id' => $beforeMax]))[0] -> id) != $beforeMin; $i++) {
        $beforeMax = $result[$i][count($result[$i])-1] -> id;	// 次のページの先頭IDを確保
        $beforeMin = $result[$i][0] -> id;
        array_pop($result[$i]);	// 次のページは現在のページの最後のツイートが先頭になるので現在のページの最後のツイートは削除し重複を回避
    }

    $addIllust = new archive;
    for ($i=0; $i<count($result); $i++) {
        for ($j=0; $j<count($result[$i])-1; $j++) {	//'-1'は末尾に射精ツイートが来くときに次のイラストが取得できなくなる事象を回避するため
            if ((($tailOfShaseiAmount = strpos($result[$i][$j] -> text, '億リットル射精')) === false) || !isset($result[$i][$j + 1] -> retweeted_status)) {	// 億リットル射精かどうか判定するついでに'億'の位置を $tailOfShaseiAmount に代入 || イラストツイートがリツイートかどうか
                continue;
            }
            $addIllust -> add($connection, $result[$i][$j], $result[$i][$j + 1], $tailOfShaseiAmount);
        }
        if ((($tailOfShaseiAmount = strpos($result[$i][count($result[$i]) - 1] -> text, '億リットル射精')) !== false) || isset($result[$i + 1][0] -> retweeted_status)) {	// 射精がページをまたいだときの処理
            $addIllust -> add($connection, $result[$i][count($result[$i]) - 1], $result[$i + 1][0], $tailOfShaseiAmount);
        }
    }
    $json = json_encode($addIllust -> archive, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    if (!file_exists(ARCHIVE_FILE_NAME)) {
        touch(ARCHIVE_FILE_NAME);
    }
    file_put_contents(ARCHIVE_FILE_NAME, $json);
