# レモビ射精ロガー (Lemon beads's Ejaculation Logger)
### Collect the ejaculation by Lemon beads on Twitter.

## Requirement📝
* PHP
* [Composer](https://getcomposer.org/)
* [abraham/twitteroauth](https://github.com/abraham/twitteroauth)  
Install TwitterOAuth with Composer.

## Install💿
1. Download this repository  
`$ git clone https://git.thinaticsystem.com/ThinaticSystem/lemobe_logger.git`
2. Install TwitterOAuth
	1. Move to repository directory  
`$ cd lemobe_logger/`
	2. Install required libraries  
`$ composer install`

## Usage🐇
* **First make archive-file(lemon_beads.json)**  
`$ php build_archive.php`  
This takes long time.
* **Update archive-file**  
`$ php update_archive.php`  
*Execute this **regularly**.*  
This script hasn't implemented function of searching more than the number Twitter Search API responds at one time yet.  
Execute 'update_archive.php' before Lemon beads tweets 100 times from last execution.
* **Download illustrations ejaculated by Lemon beads**  
`$ php download_illustration.php`

## Author✍
[ThinaticSystem (しなちくシステム)](https://thinaticsystem.com/)
