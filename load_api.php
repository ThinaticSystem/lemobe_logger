<?php
    require 'vendor/autoload.php';
    use Abraham\TwitterOAuth\TwitterOAuth;


    if (!file_exists('api_key.json')) {
        echo 'レモビ射精ロガーはTwitter APIを使用するため、APIキーの登録が必要です' . PHP_EOL;
        echo 'TwitterAPIの使用権を取得し、APIキーを入力してください' . PHP_EOL;
        echo 'consumerKeyを入力してください' . PHP_EOL;
        $newKey['consumerKey'] = trim(fgets(STDIN));
        echo 'consumerSecretを入力してください' . PHP_EOL;
        $newKey['consumerSecret'] = trim(fgets(STDIN));
        echo 'accessTokenを入力してください' . PHP_EOL;
        $newKey['accessToken'] = trim(fgets(STDIN));
        echo 'accessTokenSecretを入力してください' . PHP_EOL;
        $newKey['accessTokenSecret'] = trim(fgets(STDIN));
        $json = json_encode($newKey, JSON_PRETTY_PRINT);
        touch('api_key.json');
        file_put_contents('api_key.json', $json);
    }

    $account = json_decode(file_get_contents('api_key.json'), true);
    $connection = new TwitterOAuth(
        $account['consumerKey'],
        $account['consumerSecret'],
        $account['accessToken'],
        $account['accessTokenSecret']
    );
