<?php
    define('DOWNLOAD_DIR', 'download');


    chdir(__DIR__);

    $json = file_get_contents('lemon_beads.json');
    $archive = json_decode($json);

    if (!file_exists(DOWNLOAD_DIR)) {
        mkdir(DOWNLOAD_DIR);
    }
    chdir(DOWNLOAD_DIR);

    foreach ($archive as $tweet) {
        foreach ($tweet -> illust -> image as $imageUrl) {
            $imageData = file_get_contents($imageUrl . ':orig');
            file_put_contents(basename($imageUrl), $imageData);
            sleep(3);	// やさしさのある数字
        }
    }
