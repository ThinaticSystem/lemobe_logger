<?php
    class illustStract
    {
        public $illustrator;
        public $text;
        public $image;
        public $url;
        public $embed_code;
    }

    class rootStract
    {
        public $shasei_amount;
        public $illust;
        public $created_at;
    }

    class archive
    {
        public $archive;
        private $archiveCount;

        public function __construct()
        {
            $this -> archiveCount = 0;
        }

        public function add($connection, $sourceShasei, $sourceIllust, $tailOfShaseiAmount)
        {
            $this -> archive[$this -> archiveCount] = new rootStract;
            $this -> archive[$this -> archiveCount] -> shasei_amount = substr($sourceShasei -> text, 0, $tailOfShaseiAmount) . '億リットル';
            $this -> archive[$this -> archiveCount] -> illust = new illustStract;
            $this -> archive[$this -> archiveCount] -> illust -> illustrator = $sourceIllust -> retweeted_status -> user -> screen_name;
            $this -> archive[$this -> archiveCount] -> illust -> text = $sourceIllust -> text;
            if (!isset($sourceIllust -> extended_entities)) {	// extended_entitiesが省略されるステータス
                $fullStatus = $connection -> get('statuses/show', [ 'id' => $sourceIllust -> retweeted_status -> id, 'tweet_mode' => 'extended' ]);
            } else {
                $fullStatus = $sourceIllust;
            }
            for ($imageNo=0; $imageNo<count($fullStatus -> extended_entities -> media); $imageNo++) {	// 複数枚の画像取得 ここの'media'が存在しなかったらpixivっぽい
                $this -> archive[$this -> archiveCount] -> illust -> image[$imageNo] = $fullStatus -> extended_entities -> media[$imageNo] -> media_url;
            }
            if ($this -> archive[$this -> archiveCount] -> illust -> illustrator != null) {	// URL生成に必要な情報が取得できていない場合はURL生成をスキップ
                $this -> archive[$this -> archiveCount] -> illust -> url = 'http://twitter.com/' . $sourceIllust -> retweeted_status -> user -> screen_name . '/status/' . $sourceIllust -> retweeted_status -> id;
            }
            $this -> archive[$this -> archiveCount] -> illust -> embed_code = ($connection -> get('statuses/oembed', [ 'url' => $this -> archive[$this -> archiveCount] -> illust -> url ])) -> html;
            if (!$this -> archive[$this -> archiveCount] -> illust -> embed_code) {	// statuses/oembedのAPI制限で取得できなかった場合15分待つ（180tweet/15minutes）
                echo 'waiting...(API Limit)' . PHP_EOL;
                sleep(905);
                $this -> archive[$this -> archiveCount] -> illust -> embed_code = ($connection -> get('statuses/oembed', [ 'url' => $this -> archive[$this -> archiveCount] -> illust -> url ])) -> html;
            }
            $this -> archive[$this -> archiveCount] -> created_at = $sourceShasei -> created_at;
            $this -> archiveCount++;

            return $this -> archiveCount - 1;
        }
    }
