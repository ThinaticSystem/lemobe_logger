<?php
    chdir(__DIR__);	// cron等で動かすとき用にカレントディレクトリをこのファイルのディレクトリーに指定

    include('load_api.php');
    require('manage_archive.php');

    define('LEMON_BEADS', 1234240856);	// レモビさんのuserId
    define('ARCHIVE_FILE_NAME', 'lemon_beads.json');	// ログのファイル名


    if (!file_exists(ARCHIVE_FILE_NAME)) {
        exit(ARCHIVE_FILE_NAME . 'を読み込めません' . PHP_EOL . 'build_archive.php' . 'を実行して' . ARCHIVE_FILE_NAME . 'を生成してください。');
    }
    $readFile = file_get_contents(ARCHIVE_FILE_NAME);
    $archive = json_decode($readFile);

    $result = $connection -> get('statuses/user_timeline', [ 'user_id' => LEMON_BEADS, 'count' => 100 ]);

    $addIllust = new archive;
    for ($i=0; $i<count($result) - 1; $i++) {
        if ((($tailOfShaseiAmount = strpos($result[$i] -> text, '億リットル射精')) === false) || !isset($result[$i+1] -> retweeted_status)) {	// 億リットル射精かどうか判定するついでに'億'の位置を $tailOfShaseiAmount に代入 || イラストツイートがリツイートかどうか
            continue;
        }
        if (strtotime($result[$i] -> created_at) <= strtotime($archive[0] -> created_at)) {	// 更新確認（API制限回避）
            break;
        }
        $currentArchiveKey = $addIllust -> add($connection, $result[$i], $result[$i + 1], $tailOfShaseiAmount);
    }
    if (isset($currentArchiveKey)) {	// 更新がなかった場合$currentArchiveKeyは宣言されていない
        for ($i=$currentArchiveKey; $i>=0; $i--) {	// 更新があった場合$currentArchiveKeyには差分の末尾キーが入っている
            array_unshift($archive, $addIllust -> archive[$i]);
        }
        $writeFile = json_encode($archive, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        file_put_contents(ARCHIVE_FILE_NAME, $writeFile);
        touch('update_flag');
    } else {
        echo '更新なし' . PHP_EOL;
    }
